let express = require("express");
const PORT = 4000;
let app = express();

app.use(express.urlencoded({extended:true}));
app.use(express.json());

let users = [
	{
		"username":"Apple",
		"password":"123"
	},
	{
		"username":"Mary",
		"password":"123"
	}
];

//1
app.get("/home", (req,res)=>res.send(`Welcome to the home page.`));

//2
app.post("/signup", (req,res) => {

	if (req.body.username !== "" && req.body.password!=="") {
		res.send(`${req.body.username} successfully registered!`);
			let newUser = {
				"username": req.body.username,
				"password": req.body.password
			}
			users.push(newUser);
			console.log(users);
			//console.log(typeof users);

	} else {
		res.send(`Please input both username and password`);
	}
});

//3
app.get("/users", (req,res)=>{
	res.writeHead(200, {"Content-Type":"application/json"});
	res.write(JSON.stringify(users));
	res.end();
});

app.listen(PORT, ()=>{
	console.log(`Serve running at port ${PORT}`)
});